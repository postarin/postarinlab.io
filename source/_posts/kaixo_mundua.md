layout: post
title: Kaixo Mundua
date: 2016-10-24 17:39:16
tags:
---
[Txondorra](http://txondorra.net) elkartearentzako sortua, beste talde/kolektibo/elkarteei irekitzea ideaia ona iruditu zaigu.

Postarin.net-ek OVH-ren email zerbitzua darabil, ez da pribatasuna/segurtasuna bermatzen duen zerbitzu bat, erabilerraztasuna da funtsa.

Laster informazio gehiago. Bitartean **info(abildua)postarin.net** posta helbidera bidali galderak etab.
