var hexo = hexo || {};

function message(args, content){
  var messageClass = args[1] || '';
  content = hexo.render.renderSync({text: content, engine: 'markdown'});
  var text = '<div class="message '+ messageClass +'">' +
  '<div class="message-header">' + args[0] + '</div>' +
  '<div class="message-body">' + content + '</div></div>';

  return text;
}
hexo.extend.tag.register('message', message, {ends: true});
