var hexo = hexo || {};

function box(args, content){
  content= hexo.render.renderSync({text: content, engine: 'markdown'});
  var text = '<div class="box"><div class="content">' + content + '</div></div>';

  return text;
}
hexo.extend.tag.register('box', box, {ends: true});
