var hexo = hexo || {};

function button(args){
  var buttonClasses = args[2] || '';
  var text = '<a href="'+args[1]+'" class="button '+ buttonClasses +'">' +
  args[0] +
  '</a>';

  return text;
}
hexo.extend.tag.register('button', button);


function ibutton(args){
  var icon = args[2];
  var buttonClasses = args[3] || '';
  var text = '<a href="'+args[1]+'" class="button '+ buttonClasses +'">' +
  '<span class="icon"><i class="fa fa-'+icon+'"></i></span>' +
  '<span>' + args[0] + '</span>'+
  '</a>';

  return text;
}
hexo.extend.tag.register('ibutton', ibutton);
