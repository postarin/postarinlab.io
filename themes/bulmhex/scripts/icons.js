var hexo = hexo || {};

function icon(args){
  var iconClass = args[0] || '';
  var iconSize = args[1] || '';
  var text = '<span class="icon '+ iconSize +'">' +
  '<i class="fa fa-'+ iconClass +'"></i>' +
  '</span>';

  return text;
}
hexo.extend.tag.register('icon', icon);
